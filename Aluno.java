package Code;

public class Aluno{

//Atributos
	private String nome;
	private String matricula;

//Construtor
	public Aluno(String umNome, String umaMatricula){
		nome = umNome;
		matricula = umaMatricula;
}

//Métodos

	public void setNome(String umNome){
		nome = umNome;
	}

	public String getNome(){
		return nome;
	}

	public void setMatricula(String umaMatricula){
		matricula = umaMatricula;
	}

	public String getMatricula(){
		return matricula;
	}

}
