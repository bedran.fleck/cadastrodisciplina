package Code;
import java.util.ArrayList;

public class Disciplina{

//Atributos

	private String disciplina;

	private String codigo;

	private ArrayList<AlunoDisciplina> listaAlunosDisciplina;

//construtores
	public Disciplina(String umaDisciplina, String umCodigo){
		disciplina = umaDisciplina;
		codigo = umCodigo;
		listaPessoas = new ArrayList<AlunoDisciplina>();
	}
//métodos

	public void setDisciplina(String umaDisciplina){
		disciplina = umaDisciplina;
	}
	
	public String getDisciplina(){
		return disciplina;
	}
	
	public void setCodigo(String umCodigo){
		codigo = umCodigo;
	}
	
	public String getCodigo(){
		return codigo;
	}
	
	public String adicionar(Aluno umAluno){
		listaAlunosDisciplina.add(umAluno);
		String mensagem = "Aluno adicionado à disciplina!";
		return mensagem;
	}

	public String remover(Aluno umAluno){
		listaAlunosDisciplina.remove(umAluno);
		String aviso = "Aluno removido da disciplina!";
		return aviso;
	} 














}
