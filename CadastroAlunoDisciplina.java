package Code;
import java.io.*;


public class CadastroAlunoDisciplina{

  public static void main(String[] args) throws IOException{

//Método de captura do teclado

    InputStream entradaSistema = System.in;
    InputStreamReader leitor = new InputStreamReader(entradaSistema);
    BufferedReader leitorEntrada = new BufferedReader(leitor);
    String entradaTeclado;

//-------------------------------------------------------

//Instanciando objetos


	Aluno umAluno = new Aluno;
	Disciplina umaDisciplina = new Disciplina;
	ControleDisciplina umControleDisciplina = new ControleDisciplina;
	ControleAluno umControleAluno = new ControleAluno;
	String opcao;
//Tela de cadastro

	System.out.println("==============MENU============");
	System.out.println("1 - Cadastrar Aluno");
	System.out.println("2 - Cadastrar Disciplina");
	System.out.println("3 - Cadastrar Aluno em Disciplina");
	System.out.println("0 - Sair");
	System.out.println("Selecione a opcao desejada:");
		
	switch(opcao){
		case "1":
			System.out.println("Nome: ");
			entradaTeclado = leitorEntrada.ReadLine();
			String umNome = entradaTeclado;
			umAluno.setNome(umNome);
			
			System.out.println("Matricula: ");
			entradaTeclado = leitorEntrada.ReadLine();
			String umaMatricula = entradaTeclado;
			umAluno.setMatricula(umaMatricula);

			String mensagem = umControleAluno.adicionar(umAluno);
			System.out.println(mensagem);
			break;
		case "2":
			System.out.println("Nome da Disciplina: ");
			entradaTeclado = leitorEntrada.ReadLine();
			String umNome = entradaTeclado;
			umaDisciplina.setDisciplina(umNome);
			
			System.out.println("Matricula: ");
			entradaTeclado = leitorEntrada.ReadLine();
			String umaMatricula = entradaTeclado;
			umaDisciplina.setMatricula(umaMatricula);

			String mensagem = umControleDisciplina.adicionar(umaDisciplina);
			System.out.println(mensagem);
			break;
		case "3":
			System.out.println("Nome da Disciplina: ");
			entradaTeclado = leitorEntrada.ReadLine();
			String umNome = entradaTeclado;
			Disciplina umaDisciplina = umControleDisciplina.pesquisar(umNome);

			if (Disciplina umaDisciplina == null) {
				System.out.println("Disciplina não cadastrada!\nAbortando Processo!!")
				break;
			}
			else {
				System.out.println("Nome do aluno: ")
				entradaTeclado = leitorEntrada.ReadLine();
				String nome = entradaTeclado;
				Aluno umAluno = umControleAluno.pesquisar(nome);

				if (Aluno umAluno == null){
					System.out.println("Aluno não cadastrado!\nAbortando Processo!!")
					break;
				}
				else {
					umaDisciplina.adicionar(umAluno);
					System.out.println(mensagem);
				}
			}
			break;
	}

}
