package Code;
import java.util.ArrayList;

public class ControleAluno{
//Atributos
	private ArrayList<Aluno> listaAlunos;


//Construtor
	public ControleAluno() {
        listaAlunos = new ArrayList<Aluno>();
   }

//Métodos

	public String adicionar(Aluno umAluno){
		listaAlunos.add(umAluno);
		String mensagem = "Aluno adicionado.";
		return mensagem;
	}
	
	public String remover(Aluno umAluno){
		listaAlunos.remove(umAluno);
		String aviso = "Aluno removido";
		return aviso;
	}

	public Aluno pesquisar(Aluno umAluno){
		for (Aluno umAluno : listaAlunos){
			if (umAluno.getNome().equalsIgnoreCase(nome)){
				return umAluno;
			}
		}return null;
	}
}
